<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    //ver la pagina donde estan los usuarios
    Route::get('/users', 'UserController@index');

    //mostrar formulario de usuario nuevo
    Route::get('/users/create', 'UserController@create');

    //guardar usuario nuevo
    Route::post('/users', 'UserController@store');

    //mostrar el formulario de actualizar usuario
    Route::get('/users/{id}/edit' , 'UserController@edit');

    //guardar el usuario actualizado
    Route::put('/user/{id}/', 'UserController@uptade');

    //eliminar usuario
    Route::delete('/users/{id}' , 'UserController@destroy');

});

